<?php

namespace App\DataFixtures;

use App\Entity\Tweet;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LikeFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        // Fetch tous les Users
        $users = $manager->getRepository(User::class)->findAll();
        $usersCount = count($users);

        // Fetch tous les Tweet
        $tweets = $manager->getRepository(Tweet::class)->findAll();

        // Pour chaque Tweet,
        foreach ($tweets as $tweet) {

            for ($i = 0; $i < rand(0, $usersCount); $i++) {

                // on prend un User aleatoire
                $randomIndex = array_rand($users);
                $randomUser = $users[$randomIndex];

                $randomUser->likeTweet($tweet);
            }
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            TweetFixtures::class,
        ];
    }
}
