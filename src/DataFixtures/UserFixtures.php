<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $hash = $this->encoder->encodePassword($user, 'azeaze');
        $user
            ->setUsername('pierre')
            ->setFullName('Pierre Guillaume')
            ->setPassword($hash)
            ->setEmail('pierre.guillaume.infrep@gmail.com')
            ->setBirthDate(new \DateTime("1988-01-01T00:00:00.000000Z"));
        $manager->persist($user);


        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 10; $i++) {
            $user = new User();

            $hash = $this->encoder->encodePassword($user, 'azeaze');

            $user
                ->setUsername($faker->userName)
                ->setFullName($faker->firstName . " " . $faker->lastName)
                ->setPassword($hash)
                ->setEmail($faker->email)
                ->setBirthDate($faker->dateTimeThisCentury);
            $manager->persist($user);
        }

        $manager->flush();
    }
}
