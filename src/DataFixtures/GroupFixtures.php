<?php

namespace App\DataFixtures;

use App\Entity\Group;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use function rand;

class GroupFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $faker = Factory::create('fr_FR');

        for ($i = 0; $i < 5; $i++) {
            $randomUser = $manager->getRepository(User::class)
                ->randomUser();

            $group = new Group();
            $group->setNameGroup($faker->name);
            $group->setAuhtor($randomUser);

            for ($j = 0; $j < rand(2, 6); $j++) {
                $randomAttend = $manager->getRepository(User::class)
                    ->randomUser();

                $group->addAttende($randomAttend);
            }

            $manager->persist($group);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixtures::class,
            TweetFixtures::class,
        ];
    }

}
