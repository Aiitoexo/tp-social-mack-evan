<?php

namespace App\Controller;

use App\Entity\Tweet;
use App\Entity\User;
use App\Repository\TweetRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * CRUD : Create Read Update Delete
 *
 * Convention REST : pour les opérations de CRUD
 *
 * [GET]    /tweets     => READ la liste de tous les Tweets
 * [GET]    /tweet/{id} => READ le Tweet correspondant a id
 * [POST]   /tweets     => CREATE créer un Tweet
 * [PUT]    /tweet/{id} => UPDATE le Tweet correspondant a id
 * [DELETE] /tweet/{id} => Delete le Tweet correspondant a id
 *
 */
class TweetController extends AbstractController
{
    /**
     * On utilise le mécanisme de paramètres de l'URL de Symfony
     * @see https://symfony.com/doc/current/routing.html#route-parameters
     *
     * @Route("/tweet/{id}", name="app_tweet_single")
     * @param int $id
     * @return Response
     */
    public function readOne(int $id): Response
    {
        /**
         * @var TweetRepository $repo
         */
        $repo = $this->getDoctrine()->getRepository(Tweet::class);

        $parentTweet = $repo->find($id);

        // On doit gérer le cas où on n'a pas trouvé
        if (empty($parentTweet)) {
            throw new NotFoundHttpException("Le tweet d'id : $id n'existe pas.");
        }

        $children = $repo->findByParent($parentTweet);

        return $this->render('tweet/single.html.twig', [
            'tweet' => $parentTweet,
            'comments' => $children
        ]);
    }


    /**
     * @Route("/tweet/{id}/like", name="app_tweet_like")
     * @param int $id
     * @return Response
     */
    public function toggleLike(int $id, Request $request): Response {

        // On récupère l'utilisateur connecté
        /**
         * @var User $user
         */
        $user = $this->getUser();

        // On récupère le tweet demandé
        $tweet = $this->getDoctrine()->getRepository(Tweet::class)->find($id);

        // ON VÉRIFIE QUE CE TWEET EXISTE !!!
        if (empty($tweet)) {
            throw new NotFoundHttpException("Le tweet d'id : $id n'existe pas.");
        }

        // On modifie la relation
        if ($user->doesLike($tweet)) {
            $user->unlikeTweet($tweet);
        } else {
            $user->likeTweet($tweet);
        }

        // On enregistre en BDD
        $this->getDoctrine()->getManager()->flush();


        // On retourne la réponse du serveur

        // On essaye de rediriger sur la route d'ou l'on vient
        $referer = $request->headers->get('referer');
        if (!empty($referer)) { // le referer n'est peut être pas envoyé par le client !!
            $url = $referer . "#tweet-" .$id; // on ajoute l'ancre pour régler le niveau du scroll
            return $this->redirect($url);
        }

        // sinon on redirige vers la page single
        return $this->redirectToRoute("app_tweet_single", ['id' => $id]);

    }
}

