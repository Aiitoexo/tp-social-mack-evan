<?php

namespace App\Controller;

use App\Entity\Tweet;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class UserController extends AbstractController
{
    /**
     * @Route("/members", name="app_user_list")
     */
    public function list(): Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('user/list.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/groupe", name="app_groupe")
     */
    public function group(): Response
    {
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();

        return $this->render('user/group.html.twig', ['users' => $users]);
    }

    /**
     * @Route("/user/{id}", name="app_user_profile")
     * @param int $id
     * @return Response
     */
    public function profile(int $id): Response
    {
        /**
         * @var User $user
         */
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);

        if (empty($user)) {
            throw new NotFoundResourceException("Cet utilisateur n'existe pas");
        }

        $tweets = $this->getDoctrine()->getRepository(Tweet::class)->findByAuthor($user);

        return $this->render('user/profile.html.twig', [
            'user' => $user,
            'tweets' => $tweets,
        ]);
    }

    /**
     * @Route("/me", name="app_current_user_profile")
     */
    public function me(): Response
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        return $this->redirectToRoute('app_user_profile', ['id' => $user->getId()]);
    }

    /**
     * @Route("/create-group", name="app_createGroup")
     */
    public function createGroupe() {
        return $this->render("user/createGroup.html.twig");
        // return new Response("Mentions légales");
    }
}
