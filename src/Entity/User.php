<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"username"}, message="There is already an account with this username")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fullName;

    /**
     * @ORM\Column(type="date")
     */
    private $birthDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * List of Tweets authored by this User
     * @ORM\OneToMany(targetEntity=Tweet::class, mappedBy="author")
     */
    private $tweets;

    /**
     * List of Tweets liked by this User
     * @ORM\ManyToMany(targetEntity=Tweet::class, inversedBy="likers")
     * @ORM\JoinTable("user_like_tweet")
     */
    private $likedTweets;

    /**
     * @ORM\ManyToMany(targetEntity=Group::class, mappedBy="author")
     */
    private $groups;

    /**
     * @ORM\OneToMany(targetEntity=Group::class, mappedBy="auhtor")
     */
    private $group_author;

    /**
     * @ORM\ManyToMany(targetEntity=Group::class, mappedBy="attende")
     */
    private $attende_group;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->tweets = new ArrayCollection();
        $this->likedTweets = new ArrayCollection();
        $this->groups = new ArrayCollection();
        $this->group_author = new ArrayCollection();
        $this->attende_group = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(?string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|Tweet[]
     */
    public function getTweets(): Collection
    {
        return $this->tweets;
    }

    public function addTweet(Tweet $tweet): self
    {
        if (!$this->tweets->contains($tweet)) {
            $this->tweets[] = $tweet;
            $tweet->setAuthor($this);
        }

        return $this;
    }

    public function removeTweet(Tweet $tweet): self
    {
        if ($this->tweets->removeElement($tweet)) {
            // set the owning side to null (unless already changed)
            if ($tweet->getAuthor() === $this) {
                $tweet->setAuthor(null);
            }
        }

        return $this;
    }

    public function getAge() {

        if (!$this->birthDate) return null;

        $now = new \DateTime();

        $difference = $now->diff($this->birthDate);

        return $difference->y;

    }

    /**
     * @return Collection|Tweet[]
     */
    public function getLikedTweets(): Collection
    {
        return $this->likedTweets;
    }

    public function likeTweet(Tweet $likedTweet): self
    {
        if (!$this->likedTweets->contains($likedTweet)) {
            $this->likedTweets[] = $likedTweet;
        }

        return $this;
    }

    public function unlikeTweet(Tweet $likedTweet): self
    {
        $this->likedTweets->removeElement($likedTweet);

        return $this;
    }

    public function doesLike(Tweet $tweet): bool {
        return $this->likedTweets->contains($tweet);
    }

    /**
     * @return Collection|Group[]
     */
    public function getGroupAuthor(): Collection
    {
        return $this->group_author;
    }

    public function addGroupAuthor(Group $groupAuthor): self
    {
        if (!$this->group_author->contains($groupAuthor)) {
            $this->group_author[] = $groupAuthor;
            $groupAuthor->setAuhtor($this);
        }

        return $this;
    }

    public function removeGroupAuthor(Group $groupAuthor): self
    {
        if ($this->group_author->removeElement($groupAuthor)) {
            // set the owning side to null (unless already changed)
            if ($groupAuthor->getAuhtor() === $this) {
                $groupAuthor->setAuhtor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Group[]
     */
    public function getAttendeGroup(): Collection
    {
        return $this->attende_group;
    }

    public function addAttendeGroup(Group $attendeGroup): self
    {
        if (!$this->attende_group->contains($attendeGroup)) {
            $this->attende_group[] = $attendeGroup;
            $attendeGroup->addAttende($this);
        }

        return $this;
    }

    public function removeAttendeGroup(Group $attendeGroup): self
    {
        if ($this->attende_group->removeElement($attendeGroup)) {
            $attendeGroup->removeAttende($this);
        }

        return $this;
    }

}
