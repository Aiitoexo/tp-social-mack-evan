<?php

namespace App\Entity;

use App\Repository\GroupRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;

/**
 * @ORM\Entity(repositoryClass=GroupRepository::class)
 * @ORM\Table(name="`group`")
 */
class Group
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */

    private $name_group;

    /**
     * @ORM\OneToMany(targetEntity=Tweet::class, mappedBy="group_user")
     */
    private $tweets;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="group_author")
     * @ORM\JoinColumn(nullable=false)
     */
    private $auhtor;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, inversedBy="attende_group")
     */
    private $attende;

    public function __construct()
    {
        $this->author = new ArrayCollection();
        $this->attendees = new ArrayCollection();
        $this->tweets = new ArrayCollection();
        $this->attende = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setNameGroup(string $name_group): self
    {
        $this->name_group = $name_group;

        return $this;
    }

    /**
     * @return Collection|Tweet[]
     */
    public function getTweets(): Collection
    {
        return $this->tweets;
    }

    public function addTweet(Tweet $tweet): self
    {
        if (!$this->tweets->contains($tweet)) {
            $this->tweets[] = $tweet;
            $tweet->setGroupUser($this);
        }

        return $this;
    }

    public function removeTweet(Tweet $tweet): self
    {
        if ($this->tweets->removeElement($tweet)) {
            // set the owning side to null (unless already changed)
            if ($tweet->getGroupUser() === $this) {
                $tweet->setGroupUser(null);
            }
        }
    }

    public function getNameGroup(): ?string
    {
        return $this->name_group;
    }

    public function getAuhtor(): ?User
    {
        return $this->auhtor;
    }

    public function setAuhtor(?User $auhtor): self
    {
        $this->auhtor = $auhtor;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }


    /**
     * @return Collection|User[]
     */
    public function getAttende(): Collection
    {
        return $this->attende;
    }

    public function addAttende(User $attende): self
    {
        if (!$this->attende->contains($attende)) {
            $this->attende[] = $attende;
        }

        return $this;
    }

    public function removeAttende(User $attende): self
    {
        $this->attende->removeElement($attende);

        return $this;
    }


}
