<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210126062427 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_like_tweet RENAME INDEX idx_dfa4f163a76ed395 TO IDX_2D65E530A76ED395');
        $this->addSql('ALTER TABLE user_like_tweet RENAME INDEX idx_dfa4f1631041e39b TO IDX_2D65E5301041E39B');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE user_like_tweet RENAME INDEX idx_2d65e530a76ed395 TO IDX_DFA4F163A76ED395');
        $this->addSql('ALTER TABLE user_like_tweet RENAME INDEX idx_2d65e5301041e39b TO IDX_DFA4F1631041E39B');
    }
}
